import React, { Component } from "react";
import { View, Button, Text } from "react-native";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSigninInProgress: false,
      userInfo: [],
      uid: "",
      email: ""
    };
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      // scopes: ["https://www.googleapis.com/auth/drive.readonly"],
      webClientId:
        "265961694803-rj8pcqesnpgbi89487fmcvpcj3gpvsn0.apps.googleusercontent.com",
      offlineAccess: true // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  }

  async componentDidMount() {
    this._configureGoogleSignIn();
    await this._getCurrentUserInfo();
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>
          {this.state.email} {this.state.uid}
        </Text>

        {this.state.uid ? (
          <Button onPress={this._signOut} title="Cerrar session" />
        ) : (
          <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={this._signIn}
            disabled={this.state.isSigninInProgress}
          />
        )}
      </View>
    );
  }

  _signIn = async () => {
    this._configureGoogleSignIn();

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this.setState({ userInfo });
      this.setState({ email: userInfo.user.email });
      this.setState({ uid: userInfo.user.id });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log(error);
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.log(error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log(error);
      } else {
        // some other error happened
      }
    }
  };

  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ email: null });
      this.setState({ uid: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    this.setState({ isLoginScreenPresented: !isSignedIn });
    console.log(isSignedIn);
  };

  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo });
      this.setState({ email: userInfo.user.email });
      this.setState({ uid: userInfo.user.id });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        // user has not signed in yet
      } else {
        // some other error
      }
    }
  };
}
