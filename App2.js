import React, { Component } from 'react';
import { View, Button, Text } from 'react-native';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';




export default class Login extends Component {

  constructor(props) {
    super(props);
  
    this.state = {isSigninInProgress:false, userInfo:[]};
  }


  componentDidMount() {
 
  this._getCurrentUserInfo();
     
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>

  <GoogleSigninButton
    style={{ width: 192, height: 48 }}
    size={GoogleSigninButton.Size.Wide}
    color={GoogleSigninButton.Color.Dark}
    onPress={this._signIn}
    disabled={this.state.isSigninInProgress} /> 
    

 <Text>{JSON.stringify(this.state)}</Text>
      </View>
    );
  }

 
 _signIn = async () => {


      GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/drive.readonly'],
   forceConsentPrompt: true, 
        webClientId: '265961694803-rj8pcqesnpgbi89487fmcvpcj3gpvsn0.apps.googleusercontent.com',
        offlineAccess: true // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    this.setState({ userInfo });
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {

      console.log(error);
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (f.e. sign in) is in progress already
         console.log(error);
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
         console.log(error);
    } else {
      // some other error happened
    }
  }
}; 


_getCurrentUserInfo = async () => {
  try {
    const userInfo = await GoogleSignin.signInSilently();
    this.setState({ userInfo });
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_REQUIRED) {
      // user has not signed in yet
    } else {
      // some other error
    }
  }
};
   
};